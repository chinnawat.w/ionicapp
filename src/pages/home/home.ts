import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { InboxPage } from '../inbox/inbox';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  onpush() {
    console.log("test mouse click");
    this.navCtrl.push(InboxPage);
  }

}
